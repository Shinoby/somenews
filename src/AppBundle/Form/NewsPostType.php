<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsPostType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Заголовок новости'])
            ->add('textPreview',TextareaType::class, ['label' => 'Превью текст новости'])
            ->add('textArticle', TextareaType::class, ['label' => 'Полный текст новости'])
            ->add('alias', TextType::class, ['label' => 'Url новости'])
            ->add('img', FileType::class, ['label' => 'Изображение', 'required' => false])
            ->add('save', SubmitType::class, ['label' => 'Создать ноость'])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\NewsPost',
        ));
    }
}