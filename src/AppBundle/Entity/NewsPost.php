<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="NewsPost")
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity("alias", message="Алиас новости должен быть уникальным")
 */
class NewsPost
{

    /**
     *  @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->dateCreate = new \DateTime('now');
    }

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $newsPostID;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $alias;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     */
    private $textPreview;

    /**
     * @ORM\Column(type="text")
     */
    private $textArticle;

    /**
     * @ORM\Column(type="string")
     *
     * @Assert\File(mimeTypes={ "image/jpeg", "image/png" }, mimeTypesMessage="Допустимые форматы изображения: jpeg, png")
     */
    private $img;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    /**
     * Get newsPostID
     *
     * @return integer
     */
    public function getNewsPostID()
    {
        return $this->newsPostID;
    }

    /**
     * Set alias
     *
     * @param string $alias
     *
     * @return NewsPost
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * Get alias
     *
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return NewsPost
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set textPreview
     *
     * @param string $textPreview
     *
     * @return NewsPost
     */
    public function setTextPreview($textPreview)
    {
        $this->textPreview = $textPreview;

        return $this;
    }

    /**
     * Get textPreview
     *
     * @return string
     */
    public function getTextPreview()
    {
        return $this->textPreview;
    }

    /**
     * Set textArticle
     *
     * @param string $textArticle
     *
     * @return NewsPost
     */
    public function setTextArticle($textArticle)
    {
        $this->textArticle = $textArticle;

        return $this;
    }

    /**
     * Get textArticle
     *
     * @return string
     */
    public function getTextArticle()
    {
        return $this->textArticle;
    }

    /**
     * Set img
     *
     * @param string $img
     *
     * @return NewsPost
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string
     */
    public function getImg()
    {
        return $this->img;
    }

    public function getImgFileName(){
        if(!$this->img) return "news-default.jpg";
        return $this->img instanceof File ? $this->img->getFileName() : $this->img;
    }


    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return NewsPost
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }
}
