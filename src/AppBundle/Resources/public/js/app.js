(function(){

    function _setupPagination(){
        $('.pagination a').click(function(e){
            e.preventDefault();
            var page = $(this).data('page');
            _loadNews(page);
        })
    }

    function _loadNews(page){
        $('.pagination ul li').removeClass('active');
        $('[data-page="'+page+'"]').parent().addClass('active');
        $('#news-loader').show();
        $('#news-block .news-list').html('');
        return $.get('/news',{page: page})
            .done(function(response){
                $('#news-block').html(response);
                $('#news-loader').hide();
                _setupPagination();
            })
    }

    function _setupAliasAutoFill(){
        var _manualAlias = $('#news_post_alias').val() == '' ? false : true;
        $('#news_post_title').on('keyup', function(){
            if(!_manualAlias) {
                var title = $(this).val();
                $('#news_post_alias').val(translit(title));
            }
        });
        $('#news_post_alias').on('keyup', function(){
            _manualAlias = true;
        });
    }




    function translit(text){
        if(!text) return '';
        var space = '';
        var text = text.toLowerCase();
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
            'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
            'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh','ъ': space, 'ы': 'y', 'ь': space, 'э': 'e', 'ю': 'yu', 'я': 'ya',
            ' ': '-', '_': '-', '`': space, '~': space, '!': space, '@': space,
            '#': space, '$': space, '%': space, '^': space, '&': space, '*': space,
            '(': space, ')': space,'-': space, '\=': space, '+': space, '[': space,
            ']': space, '\\': space, '|': space, '/': space,'.': space, ',': space,
            '{': space, '}': space, '\'': space, '"': space, ';': space, ':': space,
            '?': space, '<': space, '>': space, '№':space
        }

        var result = '';
        var curent_sim = '';

        for(i=0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if(transl[text[i]] != undefined) {
                if(true){
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }


        return result;

    }

    _loadNews();
    _setupAliasAutoFill();

})();