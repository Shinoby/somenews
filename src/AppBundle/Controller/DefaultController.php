<?php

namespace AppBundle\Controller;

use AppBundle\Entity\NewsPost;
use AppBundle\Lib\Pagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', []);
    }

    /**
     * @Route("/news", name="newsList")
     */
    public function newsListAction(Request $request)
    {
        $page = $request->get('page');

        if(!$page) $page = 0;
        $totalCount =$this->getDoctrine()
            ->getRepository('AppBundle:NewsPost')
            ->createQueryBuilder('p')
            ->select('count(p.newsPostID)')
            ->getQuery()
            ->getSingleScalarResult();

        $perPage = $this->getParameter('news_per_page');
        $pagination = new Pagination($page, $totalCount, $perPage);

        $em =$this->getDoctrine()->getManager();
        $query = $em->createQuery(
                        'SELECT p
                            FROM AppBundle:NewsPost p
                            ORDER BY p.dateCreate DESC'
                    )
            ->setMaxResults($perPage)
            ->setFirstResult($page * $perPage);
        $newsPosts = $query->getResult();
        return $this->render('default/newsTable.html.twig', [
            'pagesList' => $pagination->getPagesList(),
            'page' => $page,
            'newsPosts' => $newsPosts
        ]);
    }


    /**
     * @Route("/read/{alias}", name="read")
     */
    public function readAction($alias, Request $request)
    {


        $repo = $this->getDoctrine()
                ->getRepository('AppBundle:NewsPost');
        $post = $repo->findOneBy(['alias' => $alias]);

        if(!$post){
            $this->createNotFoundException('not found news');
        }else{
            return $this->render('default/newsRead.html.twig', [
                'post' => $post
            ]);
        }


    }
}
