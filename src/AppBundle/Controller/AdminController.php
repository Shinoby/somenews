<?php

namespace AppBundle\Controller;

use AppBundle\Form\NewsPostType;
use Proxies\__CG__\AppBundle\Entity\NewsPost;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{

    /**
     * @Route("/admin", name="adminIndex")
     */
    public function indexAction(Request $request)
    {

        return $this->render('admin/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/admin/news/list", name="adminListNews")
     */
    public function listNewsAction(Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:NewsPost');
        $newsPosts = $repo->findAll();
        return $this->render('admin/news_list.html.twig', [
            'newsPosts' => $newsPosts
        ]);

    }

    /**
     * @Route("/admin/news/edit/{id}", name="adminEditNews")
     * @Route("/admin/news/edit", name="adminCreateNews")
     */
    public function editNewsAction($id = false, Request $request)
    {
        if($id) {
            $repo = $this->getDoctrine()
                ->getRepository('AppBundle:NewsPost');
            $post = $repo->find($id);
            if($post->getImg()) {
                $post->setImg(
                    new File($this->getParameter('newsImg_directory') . '/' . $post->getImg())
                );
            }
        }else{
            $post = new NewsPost();
        }

        if($post)
            $this->createNotFoundException("Новость не найдена");

        $form = $this->createForm(NewsPostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $post->getImg();
            if($file) {
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();
                $file->move(
                    $this->getParameter('newsImg_directory'),
                    $fileName
                );
            }else{
                $fileName = '';
            }

            $post->setImg(
                $fileName
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('adminListNews');
        }

        return $this->render('admin/news_edit.html.twig', array(
            'form' => $form->createView(),
            'post' => $post
        ));
    }

    /**
     * @Route("/admin/news/delete/{id}", name="adminDeleteNews")
     */
    public function deleteNewsAction($id, Request $request)
    {
        $repo = $this->getDoctrine()
            ->getRepository('AppBundle:NewsPost');
        $post = $repo->find($id);

        if($post)
            $this->createNotFoundException("Новость не найдена");

        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        return $this->redirectToRoute('adminListNews');

    }


}
