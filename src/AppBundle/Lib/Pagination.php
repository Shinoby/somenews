<?php

namespace AppBundle\Lib;

class Pagination
{
    private $totalPages;
    private $currentPage;
    private $recordsPerPage;

    public function __construct($page, $totalcount, $rpp)
    {
        $this->recordsPerPage=$rpp;
        $this->currentPage=$page;
        $this->totalPages=$this->setTotalPages($totalcount, $rpp);
    }

    private function setTotalPages($totalcount, $rpp)
    {
        if ($rpp == 0)
        {
            $rpp = 20; // In case we did not provide a number for $rpp
        }

        $this->totalPages=ceil($totalcount / $rpp);
        return $this->totalPages;
    }

    public function getTotalPages()
    {
        return $this->totalPages;
    }

    public function getPagesList()
    {
        $pageCount = $this->totalPages;

        $result = [];
        $i = 0;
        while($i<$pageCount){
            $result[] = $i;
            $i++;
        }
        return $result;
    }

}

?>